Install BOCHS 2.6 on Ubuntu
===


### Make `install.sh` executable;
Browse to the directory where install.sh is located. 
Make it executable by issuing the cmd:
```
$ chmod +x ./install.sh
```

Run the file: ```$ ./install.sh``` and follow instructions.

### Note
This is used by the class INF-2201 Operating system fundamentals. Only tested under Ubuntu 14.04 but it may work on newer distros, or other debian based distros.
