#!/usr/bin/env bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "${DIR}"

APT_FILES=(
 	"libwxgtk3.0-0" "libwxbase3.0-0" "libsdl1.2debian" "vgabios" "seabios"
)

WGET_FILES=(
	"http://archive.tanglu.org/tanglu/pool/main/b/bochs/bochsbios_2.6-2_all.deb"
	"https://launchpad.net/ubuntu/+source/bochs/2.6-2/+build/6053938/+files/bochs-term_2.6-2_amd64.deb"
	"https://launchpad.net/ubuntu/+source/bochs/2.6-2/+build/6053938/+files/bochs-wx_2.6-2_amd64.deb"
	"https://launchpad.net/ubuntu/+source/bochs/2.6-2/+build/6053938/+files/bochs-x_2.6-2_amd64.deb"
	"https://launchpad.net/ubuntu/+source/bochs/2.6-2/+build/6053938/+files/bochs_2.6-2_amd64.deb"
	"https://launchpad.net/ubuntu/+source/bochs/2.6-2/+build/6053938/+files/bximage_2.6-2_amd64.deb"
	"https://launchpad.net/ubuntu/+source/bochs/2.6-2/+build/6053938/+files/bochs-sdl_2.6-2_amd64.deb"
)

function download_apt_packages {

	echo "Required packages from apt:"
	echo " * ${APT_FILES[@]}"

	read -p "Download them? (Y/N)" REPLY

	case ${REPLY} in
		[Yy] )
			cd "${DIR}/debs"
			apt-get download "${APT_FILES[@]}"
			cd "${DIR}"
			;;
		[Nn] )
			echo "Skipping..."
			;;
	esac
}

function download_other_packages {

	echo "Required packages from web:"
	for PKG in ${WGET_FILES[@]}; do
		SPKG=$(basename "${PKG}")
		echo " * ${SPKG}"
	done

	read -p "Download them? (Y/N)" REPLY

	case ${REPLY} in
		[Yy] )
			cd "${DIR}/debs"
			for PKG in ${WGET_FILES[@]}; do
				SPKG=$(basename "${PKG}")
				if [ -e "${SPKG}" ]; then
					rm "${SPKG}"
				fi

				wget "${PKG}"
			done
			cd "${DIR}"
			;;
		[Nn] )
			echo "Skipping..."
			;;
	esac

}

function install_debs {
	read -p "Install debs located in ./debs? (Y/N/S=show) " REPLY
	case ${REPLY} in
		[Yy] )
			sudo dpkg -i -R "${DIR}/debs"
			;;
		[Ss] )
			ls "${DIR}/debs/"
			install_debs
			;;
	esac
}

function deb_dir {
	if [ -d "${DIR}/debs" ]; then
		echo "Folder exists: ./debs"
	else
		echo "Creating folder: ./debs"
		mkdir "${DIR}/debs"
	fi
}

function clean_and_exit {
	if [ -d "${DIR}/debs" ]; then
		read -p "Remove debs (from folder, not uninstall)? (Y/N) " REPLY
		case ${REPLY} in
			[Yy] )
			rm -rf "${DIR}/debs/"
			;;
		esac
	fi
	exit
}

function remove_bochs {
	read -p "Remove old version of bochs? " REPLY
	case ${REPLY} in
		[Yy] )
			sudo apt-get purge --remove bochs*
			;;
		[Nn] )
			echo "Skipping..."
			;;
	esac
}

function main {
	echo "Installation of Bochs 2.6.2 for Ubuntu 14.04.x"
	deb_dir
	remove_bochs
	download_apt_packages
	download_other_packages
	install_debs
	clean_and_exit
}

main